package com.example.assignmentnine

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.assignmentnine.databinding.FragmentFirstRegisterBinding
import com.example.assignmentnine.databinding.FragmentHomeBinding

//
class FirstRegisterFragment : Fragment() {

    lateinit var binding: FragmentFirstRegisterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentFirstRegisterBinding.inflate(inflater, container, false)
        val view = binding.root

        setUpListeners()

        return view
    }


    private fun  setUpListeners()
    {
        binding.btnNext.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_first_register_fragment_to_second_register_fragment)
        }
    }


}