package com.example.assignmentnine

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.assignmentnine.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root

        setUpListeners()

        return view
    }


    private fun setUpListeners()
    {
        binding.btnRegister.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_home_fragment_to_first_register_fragment)
        }

        binding.btnLogin.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_home_fragment_to_login_fragment)
        }
    }


}